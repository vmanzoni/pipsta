[![Pipsta Logo](https://bitbucket.org/repo/enq8r6/images/3048173118-Logo_small.jpg)](http://www.pipsta.co.uk)

# Websites #
Pipsta Website: http://www.pipsta.co.uk

Android NFC App: https://play.google.com/store/apps/details?id=com.pipsta.pipstanfcprinter

Buy Pipsta: [here](https://bitbucket.org/ablesystems/pipsta/wiki/Buy%20Pipsta)


![1.png](https://bitbucket.org/repo/enq8r6/images/972761486-1.png)![2.png](https://bitbucket.org/repo/enq8r6/images/3013025359-2.png)![3.png](https://bitbucket.org/repo/enq8r6/images/2774827282-3.png)![4.png](https://bitbucket.org/repo/enq8r6/images/195915352-4.png)![5.png](https://bitbucket.org/repo/enq8r6/images/2304211468-5.png)![6.png](https://bitbucket.org/repo/enq8r6/images/2954606727-6.png)![7.png](https://bitbucket.org/repo/enq8r6/images/515046219-7.png)![8.png](https://bitbucket.org/repo/enq8r6/images/2863242272-8.png)![9.png](https://bitbucket.org/repo/enq8r6/images/2555673849-9.png)![10.png](https://bitbucket.org/repo/enq8r6/images/1294120718-10.png)![11.png](https://bitbucket.org/repo/enq8r6/images/3249103560-11.png)![12.png](https://bitbucket.org/repo/enq8r6/images/3920460388-12.png)![13.png](https://bitbucket.org/repo/enq8r6/images/2168889593-13.png)![14.png](https://bitbucket.org/repo/enq8r6/images/718386365-14.png)![15.png](https://bitbucket.org/repo/enq8r6/images/1445243156-15.png)![16.png](https://bitbucket.org/repo/enq8r6/images/2706236026-16.png)![17.png](https://bitbucket.org/repo/enq8r6/images/3507048202-17.png)![18.png](https://bitbucket.org/repo/enq8r6/images/2524296937-18.png)![19.png](https://bitbucket.org/repo/enq8r6/images/2798308460-19.png)![20.png](https://bitbucket.org/repo/enq8r6/images/3740787466-20.png)![21.png](https://bitbucket.org/repo/enq8r6/images/4194198172-21.png)![22.png](https://bitbucket.org/repo/enq8r6/images/293865574-22.png)

Above icon art by Icons8.
http://icons8.com
Licence URL: http://icons8.com/license

# What's New #

* **22/12/15** - Our last project for 2015 is now [here](https://bitbucket.org/ablesystems/pipsta/downloads/12_PipstaCircuitsTemplate.zip)! This simple project comes with a breadboard template for you to modify and a script to print it out. General guidance is provided [here](https://bitbucket.org/ablesystems/pipsta/wiki/Pipsta%20Circuits%20Template%20and%20Script). 

* **21/12/15** - In our penultimate offering this Christmas, we have uploaded the [drawings](https://bitbucket.org/ablesystems/pipsta/downloads/11_PipstaDrawings.zip) for the standard Pipsta housing for modders and hackers. Please also ensure you read the instructions on use [here](https://bitbucket.org/ablesystems/pipsta/wiki/Tutorials#!11-pipsta-drawings).

* **18/12/15** - Our third Pipsta Circuit is up! Get in a festive mood for a Hacky Christmas with this [Audio Player Jack Hack](https://bitbucket.org/ablesystems/pipsta/wiki/Pipsta%20Circuits%203%20-%20Audio%20Hack). The download is [here](https://bitbucket.org/ablesystems/pipsta/downloads/10_AudioHack.zip).

* **17/12/15** - We love [**Fritzing**](http://fritzing.org/home/), and are delighted to present this 'hack'! Design your own breadboard layouts and print them out using this Pipsta Circuits script -- **all on the Raspberry Pi**! Read the step-by-step [tutorial](https://bitbucket.org/ablesystems/pipsta/wiki/Fritzing%20Alpha%20to%20Pipsta%20Hack) and download the [project](https://bitbucket.org/ablesystems/pipsta/downloads/9_FritzingToPipstaHack.zip)!

* **17/12/15** - For our Beta testers: we are in the final throes on producing documentation to accompany the **Text-Only CUPS Driver**. This has now been tested on Raspbian Wheezy and Jessie. We will contact you all individually in the next few days.

* **17/12/15** - A bug was reported in the *6_HollyBerries* project affecting printing. This has now been fixed and a new version uploaded. Apologies for the error. Please download the new *6_HollyBerries.zip* (upload date 17/12/15)
* **16/12/15** - An early Christmas present for those wishing to use Pipsta with the old [Pi 1 Model B](https://bitbucket.org/ablesystems/pipsta/wiki/Pi%201%20Model%20B%20CAD)! STL files available for those with a 3d printer, and STEP files for those wanting to modify the CAD for their own purposes.

* **16/12/15** - The second *Pipsta Circuits* project is here. Make Rudolph's nose glow- with an electronics solution, and a software solution...

* **14/12/15** - We're very excited to release the first of our new *Pipsta Circuits* projects! Pipsta Circuits dramatically simplify breadboarding activities, and are a great introduction to Electronics and other STEM aspects. For more information on Pipsta Circuits, see [here](https://bitbucket.org/ablesystems/pipsta/wiki/Pipsta%20Circuits), or --if you want to try it out-- download our latest [project](https://bitbucket.org/ablesystems/pipsta/downloads/6_HollyBerries.zip) and read the [tutorial](https://bitbucket.org/ablesystems/pipsta/wiki/Pipsta%20Circuits%201%20-%20Holly%20Berries)!

* **11/12/15** - Occupy the kids with this fun snowman paper dolls [project](https://bitbucket.org/ablesystems/pipsta/downloads/5_SnowmanPaperDolls.zip)! Full instructions and code tutorial [here](https://bitbucket.org/ablesystems/pipsta/wiki/Snowman%20Paper%20Dolls).

* **09/12/15** - Use Pipsta to produce beautiful Christmas decorations and even place setting name cards with this new [project](https://bitbucket.org/ablesystems/pipsta/downloads/4_ChristmasBanner.zip) and [tutorial](https://bitbucket.org/ablesystems/pipsta/wiki/Christmas%20Banner).

* **08/12/15** - For those of you late to the advent calendar, and wanting to print-off the images from previous days, here's how you do it. From the *1_AdventCalendar* directory, bring up the terminal with **F4** and type:


```
#!python

python image_print/image_print.py 1.png
```


etc. for all the days you've missed. No peeking ahead though!

* **07/12/15** - New project uploaded! Now you've (hopefully) received your Notice of Niceness after the previous Pipsta project, it's time to send Santa a Christmas list with a techie twist. The code is [here](https://bitbucket.org/ablesystems/pipsta/downloads/3_SantaLetter.zip) and the tutorial [here](https://bitbucket.org/ablesystems/pipsta/wiki/Santa%20Letter%20Generator).

* **03/12/15**- We've uploaded a new project today! Why not overcome the anxiety of **not knowing** whether Santa considers you to be naughty or nice by querying his Santabase with your name? After considerable deliberation (if you've met or exceeded the minimum threshold of niceness) a certificate will be produced. You can then relax and enjoy the festive fun (albeit nicely.) The code is [here](https://bitbucket.org/ablesystems/pipsta/wiki/Naughty%20Nice%20Checker) and the tutorial [here](https://bitbucket.org/ablesystems/pipsta/wiki/Naughty%20Nice%20Checker).

* **02/12/15**- Day two of the Pipsta advent calendar today and (mercifully) still no chocolates*! Pipsta will print a different image each day until Christmas using a simple Python script to derive a filename from the Raspberry Pi's system date and use a separate **Python module** to print the image. This provides good 'encapsulation' and leaves the top-level script uncluttered. Get the code [here](https://bitbucket.org/ablesystems/pipsta/downloads/1_AdventCalendar.zip) and read the tutorial [here](https://bitbucket.org/ablesystems/pipsta/wiki/Advent%20Calendar).

*Check back tomorrow for another Christmas-themed project and a brand new Pipsta printing technique!*

*Pipsta is not capable of printing chocolates.

* 30/11/15 - Throughout December, we will be releasing a host of new Pipsta projects. There will be enhancements to existing projects (with a Christmas slant) and the introduction of exciting new features and applications for hobbyists, educators and parents with children to occupy!

If your interests lie in software and/or electronics, there will be plenty of projects for you to enjoy. Keep an eye on Pipsta's [Facebook page](https://www.facebook.com/pages/Pipsta/921416174536872) for updates.


# What's Next #

We are currently working on a **CUPS driver for Pipsta**. This will ultimately allow printing from applications such as Leafpad, Geany and GIMP. We intend to release previews of the driver as we complete each development stage, i.e.

* text-only, 
* graphics rendering 
* full-featured driver.

If you would like to be involved in the beta testing of this driver, please [email us](mailto:support@pipsta.co.uk).

# README #

The python scripts and the documentation in this repository have been provided
to demonstrate how the Pipsta can be used in an educational or hobbyist
environment.  The Pipsta is a based around a
[Raspberry Pi](http://www.raspberrypi.org/) and a thermal printer.

### What is this repository for? ###

* Demonstration of the Pipsta
* Documentation of the setup of the Pipsta
* Documentation of the examples
* Proposed projects
* Beta
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
    * [Assemble the Pipsta](https://bytebucket.org/ablesystems/pipsta/src/master/Documents/PIPSTA002%20-%20Model%20B+%20Assembly%20Instructions.pdf)
    * [Install Raspbian and Pipsta Software on the Raspberry Pi](https://bitbucket.org/ablesystems/pipsta/src/master/Documents/PIPSTA004%20-%20Pipsta%20First-Time%20Setup.pdf).
    * Download the [latest sources from the repository](https://bitbucket.org/ablesystems/pipsta/downloads)
      to the [Raspberry Pi](http://www.raspberrypi.org/)
    * Copy this file to `/home/pi`

    ![Alt Text](https://bitbucket.org/ablesystems/pipsta/downloads/2014-11-28-100957_640x480_scrot.png "File Manager Screenshot" =160x120)

    * Right click on the downloaded file and select `xarchiver`.  When the
    window appears select the 'Action->Extract' and then click 'Extract'.
    * Rename the folder that has just been extracted to `pipsta`.
    * Double click on the 'pipsta' folder.
    * Double click on the 'Examples' folder.
    * Double click on the 'system_scripts' folder.
    *	Press **[F4]** to bring up another instance of the LXTerminal.
    * In order to disable the standard Linux printer, thereby permitting
    communication to the Pipsta printer, and allow the default user to have
    permissions to access the printer without having to be a ‘super-user’,
    enter the following lines at the $ prompt to copy files from this folder to
    the appropriate system folders.

        > sudo cp 60-ablesystems-pyusb.rules /etc/udev/rules.d

        > sudo cp usblp_blacklist.conf /etc/modprobe.d
 
        **In each case, ensure you have typed the space before the ‘/etc’.**

    * Finally, enter the following at the $ prompt. This will allow the above
    settings to be used:

        > sudo shutdown –r now

    * The system will now restart and boot to the graphical desktop.
    * Your Raspberry Pi is now set up with everything it needs to communicate
    with the Pipsta printer over the USB connection.

* Setting up the Pipsta Printer
    1. Apply power to the printer
    1. Press the ridged area at the back of the printer to open the paper
    compartment
    1. Load paper roll into printer, observing the orientation of the paper roll
    as shown the embossed legend in the base of the paper compartment.
    1. Remove a couple of turns of paper so the glue does not affect the print
    quality or printer mechanism
    1. Close the lid
    1. Check the printer’s LED is lit green and not flashing. If the LED is
    flashing, open the printer again and ensure the paper is aligned correctly
    in the printer mechanism
    1. Check the printer is functioning by double-clicking the button on the
    printer. A self-test message should be printed.

* Testing the System
    1. In the File Manager, go to the `pipsta\Examples\1_Basic_Print` folder
    1. Open a terminal window by pressing [F4]
    1. Run a simple Python script by entering the following at the $ prompt
    (note that Linux is case-sensitive):

         > python BasicPrint.py Hello from Pipsta\\!

    1. The Pipsta should now print this message to the paper roll in simple
    text. If not, check the troubleshooting steps below in Diagnosing Basic
    Printing Problems.
    1. Congratulations: you are now printing with Pipsta!

* Next Steps
    If this is the end of your session, see the section entitled Shutting
    Pipsta Down Safely, or –to continue on to more advanced applications—
    take the next step with any of the tutorials:

    [Tutorials Index](https://bitbucket.org/ablesystems/pipsta/wiki/Tutorials)

### Diagnosing Basic Printing Problems ###
**NEW** A script (`verify_pipsta_install.py`) has been added to the root of the project to try and help diagnose any installation issues.  The script checks OS, python and python libraries then it attempts to communicate with the printer.  If you would like any changes to the script then please feel free to send a request (or a patch) to <support@pipsta.co.uk>.

~~~
python verify_pipsta_install.py
~~~

| Problem | Possible Solution |
|------------|-----------------------|
| Printer LED not illuminated | Check both the power to the printer (on the back of the Pipsta) and the USB connection from Raspberry Pi to printer |
| Printer LED flashing green | Ensure the paper is loaded correctly |
| Printer LED flashing green-off-red-off | Ensure the printer power supply is present. Whilst the Raspberry Pi and Printer can communicate with just a USB connection, printing cannot take place without the printer power being applied |
| Permission error when running python script | Ensure you copied the system files to the correct locations by opening LXTerminal and pressing [UP ARROW] on the keyboard to review the previous terminal commands. |
| IO Error: Printer not found | Enter `ls /dev/a*` at the command line to list connected devices beginning with ‘a’. If you do not see ‘Ap1400’ listed, Linux cannot see the printer. Manually check that printer USB connectors are located correctly at both ends. |
| Issue not resolved by above checks | Remove the USB connection to the printer, wait a few seconds, then replace |
| Issue not resolved by above checks | Shut-down the Pi and remove power from both the Raspberry Pi and the printer. Reconnect power to both and wait for the unit to reboot |
| Issue not resolved by above checks | Send an email to <support@pipsta.co.uk> |

### Shutting Pipsta Down Safely ###
Whilst the printer is resilient when it comes to powering down, the Raspberry Pi must undergo a strict shutdown process to avoid corrupting the Micro SD card. 

* The most straightforward method of doing this is to double-click the ‘Shutdown’ icon on the desktop.
* If you are already in LXTerminal, type `sudo shutdown –h now` to shut-down the Raspberry Pi immediately.

**Always make sure ALL activity on the Raspberry Pi’s green LED (the LED on the right) has stopped before removing the power!**

### Upgrading the pipsta Firmware ###
A new tool (the fpu) has been created to allow pipsta firmware to be installed from Linux.  This has been packaged up for Raspbian and placed in the download page of this bitbucket site (along with the new firmware).  To install the new firmware follow the instructions below.

1. Download pipsta-printer-utilities-1.1.0-Linux.deb and V9_2_04.able to your Raspberry Pi.
1. Ensure you have libusb-dev installed `sudo apt-get install libusb-dev`
1. Install the printer utilities by running `sudo dpkg -i pipsta-printer-utilities-1.1.0-Linux.deb` from the directory the file is saved in.
1. Check the install `fpu --version`
1. Check the printer is connected to the Raspberry Pi `ls /dev/ap1400`
1. Install the new firmware `fpu V9.2.04.able`

If you have any problems please contact us.

### Who do I talk to? ###
* <support@pipsta.co.uk>

### Why Not Visit ###
* [Facebook](https://www.facebook.com/pages/Pipsta/921416174536872)
* [Twitter](https://twitter.com/PipstaPrinter)
* [Youtube](https://www.youtube.com/channel/UCPkYuupnqoPXgz6yDQcf0nQ)